package n01.core;

import processing.core.PVector;

public interface ArtMethods {
// time
	void frameGoesOn();

	boolean isOver();

//
	PVector getCoordinate(int future);

	PVector getSize(int future);

	int getCount();

// back
	int getBackgroundColor();

// material
	int getAmbient();

	int getEmissive();

	int getSpecular();

	float getShininess();

// camera
	PVector getRotation();

	float getDistance();
}
