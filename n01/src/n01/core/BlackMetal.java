package n01.core;

import n01.utility.Utility;
import processing.core.PApplet;
import processing.core.PVector;

public class BlackMetal extends ArtObject {
// CONSTANTS
	protected final int MAX_DUR = 5 * 30; // in frames
	protected final int MIN_DUR = 15; // in frames
	protected final int MAX_COUNT = 60;
	protected final int MIN_COUNT = 15;
	protected final int MAX_LIGHTDUR = 6; // in frames
	protected final int MIN_LIGHTDUR = 1; // in frames

	protected final float MAX_SIZE = 1080;
	protected final float MIN_SIZE = 540;
	protected final float MAX_SPEED = 90;
	protected final float MIN_SPEED = 9;
	protected final float MAX_ADD = 270;
	protected final float MIN_ADD = 1;

	protected final float MAX_ROT = 1;
	protected final float MIN_ROT = 0.1f;
	protected final float MAX_DIS = 1920 * 2;
	protected final float MIN_DIS = 1080 * 2;

	protected float speed;
	protected float add;

	// material
	protected int ambient;
	protected int emissive;
	protected int specular;
// light
	int lightDuration;

	public BlackMetal() {
		this.ikeda = new Ikeda(1, 1, 1, 0.8f);
		this.framesLeft = 0;
		this.t = 0;
		this.isOver();
	}

	public void frameGoesOn() {
		this.framesLeft -= 1;
		this.t += 1;
	}

	public boolean isOver() {
		if (this.framesLeft <= 0) {
			PVector i1 = this.ikeda.next();
			this.framesLeft = Utility.unnorm(i1.x, this.MIN_DUR, this.MAX_DUR);
			this.count = Utility.unnorm(i1.y, this.MIN_COUNT, this.MAX_COUNT);
			this.lightDuration = Utility.unnorm(i1.z, this.MIN_LIGHTDUR, this.MAX_LIGHTDUR);
			PVector i2 = this.ikeda.next();
			float rotationX = Utility.unnorm(i2.x, this.MIN_ROT, this.MAX_ROT);
			float rotationY = Utility.unnorm(i2.y, this.MIN_ROT, this.MAX_ROT);
			this.rotation = new PVector(rotationX, rotationY);
			this.distance = Utility.unnorm(i2.z, this.MIN_DIS, this.MAX_DIS);
			PVector i3 = this.ikeda.next();
			int am = 127 + (int) (i3.x * 128);
			this.ambient = Utility.colorToInt(am, am, am);
			int em = 127 + (int) (i3.y * 128);
			this.emissive = Utility.colorToInt(em, em, em);
			int spec = 127 + (int) (i3.z * 255);
			this.specular = Utility.colorToInt(spec, spec, spec);

			PVector i4 = this.ikeda.next();
			this.size = Utility.unnorm(i4.x, this.MIN_SIZE, this.MAX_SIZE);
			this.speed = Utility.unnorm(i4.y, this.MIN_SPEED, this.MAX_SPEED);
			this.add = Utility.unnorm(i4.z, this.MIN_ADD, this.MAX_ADD);
			return true;
		} else {
			return false;
		}
	}

	public PVector getCoordinate(int future) {
		PVector ikeda = this.ikeda.next();
		float x = PApplet.sin((this.t + future) * (this.speed + 0.1f)) * this.size + (ikeda.x * this.add);
		float y = PApplet.sin((this.t + future) * (this.speed + 0.2f)) * this.size + (ikeda.y * this.add);
		float z = PApplet.sin((this.t + future) * (this.speed + 0.3f)) * this.size + (ikeda.z * this.add);
		return new PVector(x, y, z);
	}

	public PVector getSize(int future) {
		PVector ikeda = this.ikeda.next();
		float x = PApplet.sin((this.t + future) * this.speed) * this.size + (ikeda.x * this.add);
		float y = PApplet.sin((this.t + future) * this.speed) * this.size + (ikeda.y * this.add);
		float z = PApplet.sin((this.t + future) * this.speed) * this.size + (ikeda.z * this.add);
		return new PVector(x, y, z);
	}

	public int getCount() {
		return this.count;
	}

	public int getBackgroundColor() {
		return Utility.colorToInt(0, 0, 0);
	}

	public int getAmbient() {
		if (this.t % (this.lightDuration * 2) < this.lightDuration) { // TODO DIRTY HACK COPY TRIPPLE

			return this.ambient;
		} else {
			return Utility.colorToInt(0, 0, 0);
		}
	}

	public int getEmissive() {
		if (this.t % (this.lightDuration * 2) < this.lightDuration) {
			return this.emissive;
		} else {
			return Utility.colorToInt(0, 0, 0);
		}
	}

	public int getSpecular() {
		if (this.t % (this.lightDuration * 2) < this.lightDuration) {
			return this.specular;
		} else {
			return Utility.colorToInt(0, 0, 0);
		}
	}

	public float getShininess() {
		return 1.0f;
	}

	public PVector getRotation() {
		return this.rotation;
	}

	public float getDistance() {
		return this.distance;
	}
}
