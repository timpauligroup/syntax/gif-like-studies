package n01.audio;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import beads.AudioContext;
import beads.BiquadFilter;
import beads.Buffer;
import beads.Envelope;
import beads.Gain;
import beads.IIRFilter;
import beads.Mult;
import beads.OscillatorBank;
import beads.UGenChain;

public class Pad extends UGenChain {
	private OscillatorBank oscs;
	private Envelope env;
	private Gain vca;
	private IIRFilter vcf;
	private Mult maxVcfFreq;
	private float[] freqs;
	private final int nfreqs = 16;
	private final int noscs = (this.nfreqs * 2) - 1;

	public Pad(AudioContext context) {
		super(context, 0, 2);
		this.freqs = new float[this.noscs];

		// UGens
		this.env = new Envelope(context, 0);
		this.maxVcfFreq = new Mult(context, 1, 500);
		this.vcf = new BiquadFilter(context, BiquadFilter.Type.LP, this.maxVcfFreq, 1);
		this.oscs = new OscillatorBank(context, Buffer.SAW, this.noscs);
		this.vca = new Gain(context, 1, env);

		// connect
		this.maxVcfFreq.addInput(env);
		this.vcf.addInput(this.oscs);
		this.vca.addInput(this.vcf);
		this.addToChainOutput(this.vca);
	}

	public void setFrequencies(float base) {
		Stream<Float> edges = Stream.of(base, base * (this.nfreqs - 1));
		List<Float> freqs1 = IntStream.range(2, this.nfreqs - 1).mapToObj(i -> i * base).collect(Collectors.toList());
		Stream<Float> freqs2 = freqs1.stream().map(i -> i + 1);
		Float[] freqs = Stream.concat(Stream.concat(freqs1.stream(), freqs2), edges).sorted().toArray(Float[]::new);
		int i = 0;
		for (Float f : freqs) {
			this.freqs[i++] = f;
		}
		this.oscs.setFrequencies(this.freqs);
	}

	public void fade(float aim, int dur) {
		this.env.addSegment(aim, dur);
	}
}
