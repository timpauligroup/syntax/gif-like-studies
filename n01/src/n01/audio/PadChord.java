package n01.audio;

import java.util.Arrays;

import beads.Add;
import beads.AudioContext;
import beads.Gain;
import beads.UGenChain;
import n01.utility.Utility;

public class PadChord extends UGenChain {
	private Pad[] pads;
	private float root;
	private float rootroot;

	public PadChord(AudioContext context, float root) {
		super(context, 0, 2);
		this.root = root;
		this.rootroot = root;

		this.pads = new Pad[5];
		Arrays.setAll(this.pads, p -> new Pad(context));
		Add add = new Add(context, this.pads[0], this.pads[1]);
		for (int i = 2; i < 5; i++) {
			add = new Add(context, add, this.pads[i]);
		}
		Gain scale = new Gain(context, 1, 0.5f);

		scale.addInput(add);
		this.addToChainOutput(scale);
	}

	public PadChord setRoot(float root) {
		if (root > (2 * this.rootroot)) {
			this.root = (root / 2.0f);
		}
		else if (root < (this.rootroot / 2.0f)){
			this.root = (root * 2);
		}
		else {
			this.root = root;
		}

		int i = 0;
		float[] freqs = {this.root, this.root * 1.25f, this.root * 1.5f, this.root * 2, this.root * 2.5f };
		for (Pad p : this.pads) {
			p.setFrequencies(freqs[i]);
			i += 1;
		}
		return this;
	}

	public void fade(float aim, int dur) {
		for (Pad p : this.pads) {
			p.fade(aim, dur);
		}
	}

	public PadChord nextRoot(float value) {
		float[] intervalls = { 6 / 5.0f, 5 / 3.0f, 5 / 6.0f, 3 / 5.0f };
		int index = Utility.unnorm(value, 0, intervalls.length - 1);
		float intervall = intervalls[index];
		this.setRoot(this.root * intervall);
		return this;
	}

}
