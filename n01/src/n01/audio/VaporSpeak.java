package n01.audio;

import guru.ttslib.TTS;
import n01.core.ArtMethods;

public class VaporSpeak extends Thread {
	private TTS tts;
	private ArtMethods vapor;

	public VaporSpeak(TTS tts, ArtMethods vapor) {
		super();
		this.tts = tts;
		this.vapor = vapor;
	}

	public void run() {
		tts.setPitch(100);
		tts.setPitchRange(1);
		tts.setPitchShift(1);
		String speech = vapor.toString();
		tts.speak(speech);
	}

}
