package n01.audio;

import beads.AudioContext;
import beads.Envelope;
import beads.Gain;
import beads.Noise;
import beads.UGenChain;

public class BlackSpeak extends UGenChain {
	protected Envelope env;
	protected Noise noise;
	protected Gain vca;

	public BlackSpeak(AudioContext context) {
		super(context, 0, 2);

		// UGens
		this.env = new Envelope(context, 0);
		this.noise = new Noise(context);
		this.vca = new Gain(context, 1, env);

		// connect
		this.vca.addInput(this.noise);
		this.addToChainOutput(this.vca);
	}

	public void fade(float aim, int dur) {
		this.env.addSegment(aim, dur);
	}
}
